var stage =0;
function acceptInput(text){
    console.log("demo: " +text + " stage: " + stage);
    switch(stage){
        case 0 :           
            mudrender.render({
            text : "Mudrenderer demonstration. Press return to start.",   
            });
            stage = stage+1;
            break;
        case 1 :  mudrender.render({
            text : "This is rendered slowly but skippable. When you press 'return' the remainder of this message is displayed instantly and you get your prompt back. This is some more text in case your reactions are very slow.",
            speed : 50,
            });
            stage++; 
            break;
        case 2 :  mudrender.render({
            text : "This is rendered slowly and is unskippable. Fuck, this sucks",
            speed : 50,
            unskippable: true
            });
            stage++; 
            break;    
         case 3 : 
            mudrender.render({
            text : "This is fast. Next sentence will be very slow, but async. So you don't have to wait and get a prompt immediately. ",
            });
            mudrender.render({
            text : "Sooooooooooo sloooooooooooow.",
            speed : 400,
            async: true,
            nolinebreak: true    
            });
            stage++; 
            break;             
        case 4 :  
            mudrender.render({
            text : "This message is ID'd. ",
            id : "myId"
            });
            mudrender.render({
            text : "This message is not ID'd. Press 'return' when ready. ",
            });
            stage++; 
            break;
        case 5 :  mudrender.render({
            text : "This was appended to the last line via {nolinebreak}. Press 'return' when ready",
            nolinebreak : true,
            noEcho: true
            });
            stage++; 
            break;    
        case 6 :  mudrender.render({
            text : "This replaces the previous line. Press 'return' when ready",
            replaceprevious : true
            });
            stage++; 
            break;      
        case 7 :  mudrender.render({
            text : "Your input is now hidden",
            hideNextInput : true,
            noEcho : true
            });
            stage++; 
            break; 
        case 8 : 
            var msg = "Unfortunately you just pressed return so you didn't see the asterisks";
            if(text.length>0){
                msg = "But I knew you typed: " + text;
            }
            mudrender.render({
            text : msg
            });
            stage++; 
            break;
        case 9 : 
            mudrender.render({
            text : "Any ID'd line can be readdressed. The line has now been altered",
            });
            mudrender.render({
            text : "The future is so bright, I don't need my eyes to see",
            target : "myId",
            bgcolor : "rgb(150,150,150)",
            color : "rgb(255,255,255)",  
            replace : true    
            });
            stage++; 
            break;    
            
        default: mudrender.render({
            text: "End of demo",
            effect: "super-gay"
        })    
            
    }
        
    
}


/*
{
    text: //text to display
    id:   // optional id for targeting a specific message
    hideinput // optional, does one-time password-style input
    nolinebreak // optional, appends to the previous line
    speed // optional, delay for printing each character. 
    unskippable // optional, infuriating
    async // user prompt will be visible while rendering commences


}




*/


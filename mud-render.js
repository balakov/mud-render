var mudrender = (function(){
    var root;
    var inputContainer;
    var input;
    
    var userInputCallback = function(){console.log("No UserInputCallback defined :(")};
    var queue = [];

    var noEcho = false;
    
    var skipRendering = false;   
    var inProgress = false;
    var skipListener = function(e){
        if(e.keyCode == 13){
                console.log("SKIPPING");
                skipRendering = true;
        }

    }
    
    var lastLine;
    
    var rainbowtable = ['255,0,0', '255,127,0','255,255,0','127,255,0','0,255,0','0,255,255','0,127,255','0,0,255','127,0,255','255,0,127'];
    var rainbowidx =0;
    
    function init(){
        // input line
        input = document.createElement("input"); 
        input.setAttribute('type',"text");
        input.setAttribute('id',"inputLine")
        input.addEventListener("keyup", function(e){
            if(e.keyCode == 13){
                collectInput(input.value);
                e.stopPropagation();
            }
            
        });

        
        let br = document.createElement("br");
        
        inputContainer = document.createElement("div");         
        inputContainer.appendChild(br);
        inputContainer.innerHTML = '>';
        inputContainer.appendChild(input);

        root.appendChild(inputContainer);
        root.setAttribute('class', "mud-console");

        
    }
    
    function collectInput(inputText){
        console.log("User typed: " + inputText);
        // don't echo when input was hidden
        if(!noEcho){
            mudrender.renderSimple(">" + inputText);    
        }
            
        userInputCallback(inputText);
        
        input.value = '';
    }
    
    function renderDeferred(text, line, speed, callback){
        inProgress = true;

        if(text.length>0){
            if(skipRendering || !speed){
                line.innerHTML = line.innerHTML + text;
                skipRendering = false;
                inProgress = false;
                if(callback){
                    console.log("skipped deferred rendering")
                    callback();
                }
                return;
            }
            line.innerHTML = line.innerHTML + text.charAt(0);        
            setTimeout(function(){renderDeferred(text.slice(1,text.length), line, speed, callback)}, speed);
        } else {
            skipRendering = false;
            inProgress = false;
            if(callback){
                console.log("finished deferred rendering")
                callback();
            }
            return;
        }          
            
        
    }
    function rainbow(element, idx){
        if(!idx){
            var txt = element.innerHTML;
            element.innerHTML = '';
            console.log(txt);
            for(var i=0; i < txt.length; i++){
                rainbowidx%=rainbowtable.length;
                var l = document.createElement("span");
                l.style.color = "rgb("+rainbowtable[rainbowidx++]+")";
                l.innerHTML = txt.charAt(i);
                element.appendChild(l);
            }  
            setTimeout(function(){rainbow(element, 1)},200);
            
            
        } else {
            for(var i=0; i < element.childNodes.length; i++){
                rainbowidx%=rainbowtable.length;
                var l = element.childNodes[i];
                l.style.color = "rgb("+rainbowtable[rainbowidx++]+")";
               
            }  
            setTimeout(function(){rainbow(element, 1)},200);
            
        }

        
        
        
    }
    
    function unhideInput(){
        inputContainer.style.display = 'block';
    }
    function hideInput(){
        inputContainer.style.display = 'none';
    }
    
    
    return {
        create: function(docElement, userInputCallbackFunc, bufferSize){
            root = docElement;
            userInputCallback = userInputCallbackFunc;
            init();
            console.log("created");
        },
        
        renderSimple: function(text){
            var div = document.createElement('div');
            var x = document.createElement("br");
            div.appendChild(x);
            div.innerHTML = text;
            root.insertBefore(div, inputContainer);                        
        },
        render : function(msg){
            // compute target line
            var targetLine; 
            if(msg.noEcho){
                noEcho = true;
            } else {
                noEcho = false;
            }
            
            if(msg.nolinebreak){
                targetLine = lastLine;
            }else {
                
                var target = document.getElementById(msg.target);
                if(target){
                   targetLine = target;
                } else {
                    var div = document.createElement('div');
                    if(msg.id){
                        div.setAttribute('id', msg.id);
                    }
                    root.insertBefore(div, inputContainer);
                    targetLine = div; 
                    
                }
                
               
            }
            
            // if allowed, add ways for the user to skip message
            if(msg.unskippable){
                console.log("unskippable")
                document.body.removeEventListener("keyup", skipListener);
            } else {
                console.log("skippable")
                document.body.addEventListener("keyup", skipListener);
            }
            // if it is to be replaced, clear it.
            if(msg.replace){
                targetLine.innerHTML = "";
            }
            
            if(msg.color){
                targetLine.style.color = msg.color;
            }
            if(msg.bgcolor){
                targetLine.style.backgroundColor = msg.bgcolor;
            }
            
            
            // does the user receive a prompt immediately?
            if(!msg.async){
                console.log("non-async");
                hideInput();
                renderDeferred(msg.text, targetLine, msg.speed, unhideInput);                 
            } else {
                renderDeferred(msg.text, targetLine, msg.speed); 
            }

            if(msg.effect == "super-gay"){
                console.log("rainbowing " + targetLine.innerHTML)
                rainbow(targetLine);
            }

            if(msg.hideNextInput){
                input.setAttribute("type", "password");
            } else {
                input.removeAttribute("type", "password");
            }
            lastLine = targetLine;
                              
        }
        
    }

}());